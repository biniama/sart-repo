package com.bits.sart.model

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AttachmentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def upload() {

        def file = request.getFile('file')

        if(file.empty) {

            flash.message = "File cannot be empty"

        } else {

            Attachment attachment = new Attachment()

            attachment.fileName = file.originalFilename

            attachment.fullPath = grailsApplication.config.uploadFolder + attachment.filename

            file.transferTo(new File(attachment.fullPath))

            attachment.save()
        }
        redirect (action:'list')
    }

    def download(long id) {

        Attachment attachment = Attachment.get(id)

        if (null == attachment) {

            flash.message = "Document not found."
            redirect (action:'list')

        } else {

            response.setContentType("APPLICATION/OCTET-STREAM")
            response.setHeader("Content-Disposition", "Attachment;Filename=\"${attachment.fileName}\"")

            def file = new File(attachment.fullPath)
            def fileInputStream = new FileInputStream(file)
            def outputStream = response.getOutputStream()

            byte[] buffer = new byte[4096];
            int len;

            while ((len = fileInputStream.read(buffer)) > 0) {

                outputStream.write(buffer, 0, len);
            }

            outputStream.flush()
            outputStream.close()
            fileInputStream.close()
        }
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Attachment.list(params), model:[attachmentInstanceCount: Attachment.count()]
    }

    def show(Attachment attachmentInstance) {
        respond attachmentInstance
    }

    def create() {
        respond new Attachment(params)
    }

    @Transactional
    def save(Attachment attachmentInstance) {
        if (attachmentInstance == null) {
            notFound()
            return
        }

        if (attachmentInstance.hasErrors()) {
            respond attachmentInstance.errors, view:'create'
            return
        }

        attachmentInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'attachmentInstance.label', default: 'Attachment'), attachmentInstance.id])
                redirect attachmentInstance
            }
            '*' { respond attachmentInstance, [status: CREATED] }
        }
    }

    def edit(Attachment attachmentInstance) {
        respond attachmentInstance
    }

    @Transactional
    def update(Attachment attachmentInstance) {
        if (attachmentInstance == null) {
            notFound()
            return
        }

        if (attachmentInstance.hasErrors()) {
            respond attachmentInstance.errors, view:'edit'
            return
        }

        attachmentInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'attachment.label', default: 'Attachment'), attachmentInstance.id])
                redirect attachmentInstance
            }
            '*'{ respond attachmentInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Attachment attachmentInstance) {

        if (attachmentInstance == null) {
            notFound()
            return
        }

        attachmentInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'attachment.label', default: 'Attachment'), attachmentInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'attachmentInstance.label', default: 'Attachment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
