package com.bits.sart.model


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SartUserController {

    def userService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SartUser.list(params), model: [sartUserInstanceCount: SartUser.count()]
    }

    def show(SartUser sartUserInstance) {
        respond sartUserInstance
    }

    def create() {
        respond new SartUser(params)
    }

    @Transactional
    def save(SartUser sartUserInstance) {
        if (sartUserInstance == null) {
            notFound()
            return
        }

        if (sartUserInstance.hasErrors()) {
            respond sartUserInstance.errors, view: 'create'
            return
        }

        sartUserInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'sartUserInstance.label', default: 'SartUser'), sartUserInstance.id])
                redirect sartUserInstance
            }
            '*' { respond sartUserInstance, [status: CREATED] }
        }
    }

    @Transactional
    def saveSartUser() {

        File attachedFile = request.getFile('attachmentFileName')

        SartUser user = userService.saveUser(params.username, params.password, params.confirmedPassword, params.firstName, params.middleName, params.lastName, params.gender,
                params.summary, params.position, params.phoneNumber, params.email, params.street, params.woreda,
                params.cityId.toInteger(), params.highestLevelOfEducation, params.yearsOfExperience.toDouble(), params.attachmentDescription,
                params.attachmentType, attachedFile)

        if(null != user) {

            flash.message = message(code: 'user.saveUser.success.result')
            redirect action: 'index'

        } else {

            flash.message = message(code: 'user.saveUser.failed.result', default: 'Error creating User')
            redirect action: 'index'
        }
    }

    def edit(SartUser sartUserInstance) {
        respond sartUserInstance
    }

    @Transactional
    def update(SartUser sartUserInstance) {
        if (sartUserInstance == null) {
            notFound()
            return
        }

        if (sartUserInstance.hasErrors()) {
            respond sartUserInstance.errors, view: 'edit'
            return
        }

        sartUserInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SartUser.label', default: 'SartUser'), sartUserInstance.id])
                redirect sartUserInstance
            }
            '*' { respond sartUserInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SartUser sartUserInstance) {

        if (sartUserInstance == null) {
            notFound()
            return
        }

        sartUserInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SartUser.label', default: 'SartUser'), sartUserInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'sartUserInstance.label', default: 'SartUser'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
