package com.bits.sart.model

import javax.swing.text.View

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ResearchController {

    def researchService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Research.list(params), model: [researchInstanceCount: Research.count()]
    }

    def show(Research researchInstance) {
        respond researchInstance
    }

    def create() {
        respond new Research(params)
    }

    @Transactional
    def save(Research researchInstance) {
        if (researchInstance == null) {
            notFound()
            return
        }

        if (researchInstance.hasErrors()) {
            respond researchInstance.errors, view: 'create'
            return
        }

        researchInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'research.label', default: 'Research'), researchInstance.id])
                redirect researchInstance
            }
            '*' { respond researchInstance, [status: CREATED] }
        }
    }

    def edit(Research researchInstance) {
        respond researchInstance
    }

    @Transactional
    def update(Research researchInstance) {
        if (researchInstance == null) {
            notFound()
            return
        }

        if (researchInstance.hasErrors()) {
            respond researchInstance.errors, view: 'edit'
            return
        }

        researchInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'research.label', default: 'Research'), researchInstance.id])
                redirect researchInstance
            }
            '*' { respond researchInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Research researchInstance) {

        if (researchInstance == null) {
            notFound()
            return
        }

        researchInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'research.label', default: 'Research'), researchInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'research.label', default: 'Research'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def search() {

        params.max = Math.min(params.max ? params.int('max') : 10, 50)

        List<Research> searchResults = researchService.getListOfResearches(params.researchTitle, ResearchCategory.findByName(params.researchCategory))

        if(null != searchResults) {

            flash.message = message(code: 'research.search.success.result', args: [searchResults.size()])
            render(view: '/research/search', model: [searchResults : searchResults, researchInstanceCount: searchResults.size()])

        } else {

            flash.message = message(code: 'research.search.failed.result', default: 'Sorry, no research found')
            render(view: '/research/search', model: [searchResults : [], researchInstanceCount: 0])
        }
    }

    def addClient() {
        render(template:'addClient', model:[new Client(params)])
    }

    def saveClient() {

        Client client = researchService.saveClient(params.clientName, params.street, params.woreda, params.phoneNumber, params.cityName)

        if(null != client) {

            flash.message = message(code: 'research.saveClient.success.result')
            render(view: '/research/create')
        } else {

            flash.message = message(code: 'research.search.failed.result', default: 'Sorry, no research found')
            render(view: '/research/create')
        }

    }
}
