package com.bits.sart.model


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DurationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Duration.list(params), model: [durationInstanceCount: Duration.count()]
    }

    def show(Duration durationInstance) {
        respond durationInstance
    }

    def create() {
        respond new Duration(params)
    }

    @Transactional
    def save(Duration durationInstance) {
        if (durationInstance == null) {
            notFound()
            return
        }

        if (durationInstance.hasErrors()) {
            respond durationInstance.errors, view: 'create'
            return
        }

        durationInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'durationInstance.label', default: 'Duration'), durationInstance.id])
                redirect durationInstance
            }
            '*' { respond durationInstance, [status: CREATED] }
        }
    }

    def edit(Duration durationInstance) {
        respond durationInstance
    }

    @Transactional
    def update(Duration durationInstance) {
        if (durationInstance == null) {
            notFound()
            return
        }

        if (durationInstance.hasErrors()) {
            respond durationInstance.errors, view: 'edit'
            return
        }

        durationInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Duration.label', default: 'Duration'), durationInstance.id])
                redirect durationInstance
            }
            '*' { respond durationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Duration durationInstance) {

        if (durationInstance == null) {
            notFound()
            return
        }

        durationInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Duration.label', default: 'Duration'), durationInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'durationInstance.label', default: 'Duration'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
