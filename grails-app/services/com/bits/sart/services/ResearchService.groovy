package com.bits.sart.services

import com.bits.sart.model.Address
import com.bits.sart.model.City
import com.bits.sart.model.Client
import com.bits.sart.model.Research
import com.bits.sart.model.ResearchCategory
import grails.transaction.Transactional

@Transactional
class ResearchService {

    List<Research> getListOfResearches(String researchTitle, ResearchCategory researchCategory) {

        return Research.withCriteria {

            eq('title', researchTitle)
            eq('researchCategory', researchCategory)
        }
    }

}
