package com.bits.sart.services

import com.bits.sart.model.Address
import com.bits.sart.model.City
import com.bits.sart.model.Client
import grails.transaction.Transactional

@Transactional
class ClientService {

    Client saveClient(String clientName, String street, String woreda, String phoneNumber, Integer cityId) {

        Address address = new Address()
        Client client = new Client()

        address.street = street
        address.woreda = woreda
        address.phoneNumber = phoneNumber
        address.city = City.get(cityId)
        address.save(flush: true, failOnError: true)

        client.name = clientName
        client.address = address
        client.save(flush: true, failOnError: true)

        return client
    }
}
