package com.bits.sart.services

import com.bits.sart.exception.UserServiceException
import com.bits.sart.model.Address
import com.bits.sart.model.Attachment
import com.bits.sart.model.City
import com.bits.sart.model.GenderTypeEnum
import com.bits.sart.model.SartUser
import grails.transaction.Transactional

@Transactional
class UserService {

    def messageSource

    SartUser saveUser(String username, String password, String confirmedPassword, String firstName, String middleName, String lastName, String gender,
                 String summary, String position, String phoneNumber, String email, String street, String woreda,
                 Integer cityId, String highestLevelOfEducation, Double yearsOfExperience, String attachmentDescription,
                 String attachmentType, File attachmentFileName) throws UserServiceException {

        if(!password.equals(confirmedPassword)) {

            def errorMessage = messageSource.getMessage("error.user.password.does.not.match", "Password does not match. Please try again.")

            log.error(errorMessage)

            throw (new UserServiceException(errorMessage, UserServiceException.ERROR_PASSWORD_DOES_NOT_MATCH))
        }

        SartUser user = new SartUser()

        try {

            user.username = username
            user.password = password
            user.firstName = firstName
            user.middleName = middleName
            user.lastName = lastName
            user.gender = GenderTypeEnum.valueOf(gender)
            user.summary = summary
            user.position = position
            user.highestLevelOfEducation = highestLevelOfEducation
            user.yearsOfExperience = yearsOfExperience

            Address address = new Address()
            print(phoneNumber + " " + email  + " " + street + " " + woreda + " " + cityId + " " + City.get(cityId))
            address.phoneNumber = phoneNumber
            address.email = email
            address.street = street
            address.woreda = woreda
            address.city = City.get(cityId)
            address.save(flush: true, failOnError: true)

            user.address = address

            Attachment attachment = new Attachment()
            attachment.description = attachmentDescription
            attachment.fileName = attachmentFileName
            attachment.type = attachmentType
            attachment.save(flush: true, failOnError: true)

            if(null == user.attachments) {

                user.attachments = new ArrayList<Attachment>()
            }

            user.attachments.add(attachment)

            user.save(flush: true, failOnError: true)

            return user

        } catch (Exception e) {

            def errorMessage = messageSource.getMessage("error.user.cannot.be.created", "User cannot be created.")

            log.error(errorMessage)

            throw (new UserServiceException(errorMessage, UserServiceException.ERROR_USER_CANNOT_BE_CREATED))

        }

    }
}
