import java.util.Date;

import com.bits.sart.Constants
import com.bits.sart.model.User;

databaseChangeLog = {

	//Secure Admin Module
	changeSet(author: "biniama", id: "a283bc86-5c03-432b-9318-55e11edc1cd1-3") {

		preConditions(onFail: 'MARK_RAN', onFailMessage: 'Admin Module is Already Secured' ){
			sqlCheck(expectedResult: '0', "SELECT COUNT(*) FROM request_map WHERE url = '/admin**/**'")
		}

		sql("""
				INSERT INTO request_map(url, `version`, `config_attribute`)
				SELECT '/admin**/**', 0, r.authority
				FROM role r
				WHERE r.authority = '${Constants.ROLE_ADMIN}';
		""")
	}
}