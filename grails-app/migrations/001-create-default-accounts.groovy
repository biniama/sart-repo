import com.bits.sart.Constants

databaseChangeLog = {
	
	def grailsApplication
	
	// Create Admin Role
	changeSet(author: "biniama", id:"a283bc86-5c03-432b-9318-55e11edc1cd1-1") {
		
		preConditions(onFail: 'MARK_RAN', onFailMessage:'Admin role already exists.') {
			
			sqlCheck(expectedResult: '0', "SELECT COUNT(*) FROM Role WHERE authority = '${Constants.ROLE_ADMIN}'")
		}
		
		sql("""
				INSERT INTO role(authority, `version`)
				SELECT '${Constants.ROLE_ADMIN}', 0 
			""")
	}
	
	//Create Admin User and Assign Admin Role
	changeSet(author: "biniama", id: "a283bc86-5c03-432b-9318-55e11edc1cd1-2") {
		
		preConditions(onFail: 'MARK_RAN', onFailMessage: 'Admin User Already Exists' ){
			sqlCheck(expectedResult: '0', "SELECT COUNT(*) FROM `user` WHERE username = 'admin'")
		}
		
		sql("""
				ALTER TABLE `user` DROP COLUMN class;
			""")
		
		def adminUserPassword = grailsApplication.mainContext.springSecurityService.encodePassword('password')

		sql("""
				INSERT INTO `user`(username, `password`, `version`, enabled, account_expired, account_locked, password_expired, 
					`created_by_user_id`, `date_created`, `updated_by_user_id`, `last_updated`)
				SELECT 'admin', '${adminUserPassword}', 0, 1, 0, 0, 0, NULL, NOW(), NULL, NOW();
		""")
				
		sql("""
				INSERT INTO user_role(role_id, user_id)
				SELECT r.id, u.id
				FROM `user` u, role r
				WHERE u.username = 'admin' AND r.authority = '${Constants.ROLE_ADMIN}'
		""")				
	}
	
}