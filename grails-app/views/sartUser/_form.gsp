<%@ page import="com.bits.sart.model.SartUser" %>
%{--<r:require modules="bootstrap-file-upload"/>--}%
%{--
<div style="max-width:500px; border:2px solid #ccc;">
--}%

    <div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'username', 'error')} required">
	    <label for="username">
		    <g:message code="sartUser.username.label" default="Username" />
		    <span class="required-indicator">*</span>
	    </label>
	    <g:textField name="username" required="" value="${sartUserInstance?.username}"/>
    </div>

    <div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'password', 'error')} required">
        <label for="password">
            <g:message code="sartUser.password.label" default="Password" />
            <span class="required-indicator">*</span>
        </label>
        <g:passwordField name="password" required="" value=""/>
    </div>

    <div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'password', 'error')} required">
        <label for="confirmedPassword">
            <g:message code="sartUser.confirm.password.label" default="Confirm Password" />
            <span class="required-indicator">*</span>
        </label>
        <g:passwordField name="confirmedPassword" required="" value=""/>
    </div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="sartUser.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" required="" value="${sartUserInstance?.firstName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'middleName', 'error')} required">
	<label for="middleName">
		<g:message code="sartUser.middleName.label" default="Middle Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="middleName" required="" value="${sartUserInstance?.middleName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="sartUser.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" required="" value="${sartUserInstance?.lastName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'gender', 'error')} ">
    <label for="gender">
        <g:message code="sartUser.gender.label" default="Gender" />

    </label>
    <g:select name="gender" from="${com.bits.sart.model.GenderTypeEnum?.values()}" keys="${com.bits.sart.model.GenderTypeEnum.values()}"
              value="${sartUserInstance?.gender?.name().toString()}" noSelection="['': '--Choose--']" valueMessagePrefix="gender"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'summary', 'error')} ">
	<label for="summary">
		<g:message code="sartUser.summary.label" default="Summary" />
        <span class="required-indicator">*</span>
	</label>
	<g:textField name="summary" value="${sartUserInstance?.summary}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'position', 'error')} required">
	<label for="position">
		<g:message code="sartUser.position.label" default="Position" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="position" required="" value="${sartUserInstance?.position}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance.address, field: 'phoneNumber', 'error')} ">
    <label for="phoneNumber">
        <g:message code="sartUser.address.phoneNumber.label" default="Phone Number" />

    </label>
    <g:textField name="phoneNumber" value="${sartUserInstance?.address?.phoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance.address, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="sartUser.address.email.label" default="Email" />
		
	</label>
	<g:textField name="email" value="${sartUserInstance?.address?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'address', 'error')} required">
    <label for="street">
        <g:message code="sartUser.address.street.label" default="Street" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="street" maxlength="100" required="" value="${sartUserInstance?.address?.street}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance.address, field: 'woreda', 'error')} ">
    <label for="woreda">
        <g:message code="sartUser.address.woreda.label" default="Woreda" />
    </label>
    <g:textField name="woreda" value="${sartUserInstance?.address?.woreda}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance.address, field: 'city', 'error')} ">
    <label for="cityId">
        <g:message code="sartUser.address.city.label" default="City" />
    </label>
    <g:select id="cityId" name="cityId" from="${com.bits.sart.model.City.list()}" optionKey="id" optionValue="name"
              value="${sartUserInstance?.address?.city?.name}" class="many-to-one" noSelection="['null': '']"/>
</div>


<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'highestLevelOfEducation', 'error')} ">
	<label for="highestLevelOfEducation">
		<g:message code="sartUser.highestLevelOfEducation.label" default="Highest Level Of Education" />
		
	</label>
	<g:select name="highestLevelOfEducation" from="${com.bits.sart.model.LevelOfEducation?.values()}" keys="${com.bits.sart.model.LevelOfEducation.values()}"
              value="${sartUserInstance?.highestLevelOfEducation?.name()}" noSelection="['': '--Choose--']" valueMessagePrefix="levelOfEducation"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sartUserInstance, field: 'yearsOfExperience', 'error')} ">
	<label for="yearsOfExperience">
		<g:message code="sartUser.yearsOfExperience.label" default="Years Of Experience" />
		
	</label>
	<g:field type="number" name="yearsOfExperience" value="${fieldValue(bean: sartUserInstance, field: 'yearsOfExperience')}"/>
</div>

<g:render template="/attachment/form"/>

%{--<div class="fieldcontain ${hasErrors(bean: sartUserInstance.attachments, field: 'fileName', 'error')} required">
    <label for="attachmentFileName">
        <g:message code="attachment.fileName.label" default="File Name" />
        <span class="required-indicator">*</span>
    </label>
    <bsfu:fileUpload action="uploadAttachment" controller="attachment"/>
</div>--}%





