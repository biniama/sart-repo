
<%@ page import="com.bits.sart.model.SartUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'sartUser.label', default: 'SartUser')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-sartUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-sartUser" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="username" title="${message(code: 'sartUser.username.label', default: 'Username')}" />

						<g:sortableColumn property="firstName" title="${message(code: 'sartUser.firstName.label', default: 'First Name')}" />
					
						<g:sortableColumn property="middleName" title="${message(code: 'sartUser.middleName.label', default: 'Middle Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${sartUserInstanceList}" status="i" var="sartUserInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${sartUserInstance.id}">${fieldValue(bean: sartUserInstance, field: "username")}</g:link></td>

						<td>${fieldValue(bean: sartUserInstance, field: "firstName")}</td>
					
						<td>${fieldValue(bean: sartUserInstance, field: "middleName")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${sartUserInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
