
<%@ page import="com.bits.sart.model.SartUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'sartUser.label', default: 'SartUser')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-sartUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-sartUser" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list sartUser">
			
				<g:if test="${sartUserInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="sartUser.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${sartUserInstance}" field="username"/></span>
					
				</li>
				</g:if>

				<g:if test="${sartUserInstance?.firstName}">
				<li class="fieldcontain">
					<span id="firstName-label" class="property-label"><g:message code="sartUser.firstName.label" default="First Name" /></span>
					
						<span class="property-value" aria-labelledby="firstName-label"><g:fieldValue bean="${sartUserInstance}" field="firstName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.middleName}">
				<li class="fieldcontain">
					<span id="middleName-label" class="property-label"><g:message code="sartUser.middleName.label" default="Middle Name" /></span>
					
						<span class="property-value" aria-labelledby="middleName-label"><g:fieldValue bean="${sartUserInstance}" field="middleName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.lastName}">
				<li class="fieldcontain">
					<span id="lastName-label" class="property-label"><g:message code="sartUser.lastName.label" default="Last Name" /></span>
					
						<span class="property-value" aria-labelledby="lastName-label"><g:fieldValue bean="${sartUserInstance}" field="lastName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.summary}">
				<li class="fieldcontain">
					<span id="summary-label" class="property-label"><g:message code="sartUser.summary.label" default="Summary" /></span>
					
						<span class="property-value" aria-labelledby="summary-label"><g:fieldValue bean="${sartUserInstance}" field="summary"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.position}">
				<li class="fieldcontain">
					<span id="position-label" class="property-label"><g:message code="sartUser.position.label" default="Position" /></span>
					
						<span class="property-value" aria-labelledby="position-label"><g:fieldValue bean="${sartUserInstance}" field="position"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.gender}">
				<li class="fieldcontain">
					<span id="gender-label" class="property-label"><g:message code="sartUser.gender.label" default="Gender" /></span>
					
						<span class="property-value" aria-labelledby="gender-label"><g:fieldValue bean="${sartUserInstance}" field="gender"/></span>
					
				</li>
				</g:if>

                <g:if test="${sartUserInstance?.address?.street}">
                    <li class="fieldcontain">
                        <span id="street-label" class="property-label"><g:message code="address.street.label" default="Street" /></span>

                        <span class="property-value" aria-labelledby="street-label"><g:fieldValue bean="${sartUserInstance?.address}" field="street"/></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.address?.woreda}">
                    <li class="fieldcontain">
                        <span id="woreda-label" class="property-label"><g:message code="address.woreda.label" default="Woreda" /></span>

                        <span class="property-value" aria-labelledby="woreda-label"><g:fieldValue bean="${sartUserInstance?.address}" field="woreda"/></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.address?.phoneNumber}">
                    <li class="fieldcontain">
                        <span id="phoneNumber-label" class="property-label"><g:message code="address.phoneNumber.label" default="Phone Number" /></span>

                        <span class="property-value" aria-labelledby="phoneNumber-label"><g:fieldValue bean="${sartUserInstance?.address}" field="phoneNumber"/></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.address?.email}">
                    <li class="fieldcontain">
                        <span id="email-label" class="property-label"><g:message code="address.email.label" default="Email" /></span>

                        <span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${sartUserInstance?.address}" field="email"/></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.address?.city}">
                    <li class="fieldcontain">
                        <span id="city-label" class="property-label"><g:message code="address.city.label" default="City" /></span>

                        <span class="property-value" aria-labelledby="city-label"><g:link controller="city" action="show" id="${sartUserInstance?.address?.city?.id}">${sartUserInstance?.address?.city?.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.highestLevelOfEducation}">
				<li class="fieldcontain">
					<span id="highestLevelOfEducation-label" class="property-label"><g:message code="sartUser.highestLevelOfEducation.label" default="Highest Level Of Education" /></span>
					
						<span class="property-value" aria-labelledby="highestLevelOfEducation-label"><g:fieldValue bean="${sartUserInstance}" field="highestLevelOfEducation"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.yearsOfExperience}">
				<li class="fieldcontain">
					<span id="yearsOfExperience-label" class="property-label"><g:message code="sartUser.yearsOfExperience.label" default="Years Of Experience" /></span>
					
						<span class="property-value" aria-labelledby="yearsOfExperience-label"><g:fieldValue bean="${sartUserInstance}" field="yearsOfExperience"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.accountExpired}">
				<li class="fieldcontain">
					<span id="accountExpired-label" class="property-label"><g:message code="sartUser.accountExpired.label" default="Account Expired" /></span>
					
						<span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean boolean="${sartUserInstance?.accountExpired}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${sartUserInstance?.accountLocked}">
				<li class="fieldcontain">
					<span id="accountLocked-label" class="property-label"><g:message code="sartUser.accountLocked.label" default="Account Locked" /></span>
					
						<span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean boolean="${sartUserInstance?.accountLocked}" /></span>
					
				</li>
				</g:if>

                <g:if test="${sartUserInstance?.attachments?.description}">
                    <li class="fieldcontain">
                        <span id="description-label" class="property-label"><g:message code="attachment.description.label" default="Description" /></span>

                        <span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${sartUserInstance?.attachments}" field="description"/></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.attachments?.fileName}">
                    <li class="fieldcontain">
                        <span id="fileName-label" class="property-label"><g:message code="attachment.fileName.label" default="File Name" /></span>

                    </li>
                </g:if>

                <g:if test="${sartUserInstance?.attachments?.type}">
                    <li class="fieldcontain">
                        <span id="type-label" class="property-label"><g:message code="attachment.type.label" default="Type" /></span>

                        <span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${sartUserInstance?.attachments}" field="type"/></span>

                    </li>
                </g:if>

				<g:if test="${sartUserInstance?.enabled}">
				<li class="fieldcontain">
					<span id="enabled-label" class="property-label"><g:message code="sartUser.enabled.label" default="Enabled" /></span>
					
						<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${sartUserInstance?.enabled}" /></span>
					
				</li>
				</g:if>

				<g:if test="${sartUserInstance?.passwordExpired}">
				<li class="fieldcontain">
					<span id="passwordExpired-label" class="property-label"><g:message code="sartUser.passwordExpired.label" default="Password Expired" /></span>
					
						<span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean boolean="${sartUserInstance?.passwordExpired}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:sartUserInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${sartUserInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
