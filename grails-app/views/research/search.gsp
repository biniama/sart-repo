<%--
  Created by IntelliJ IDEA.
  User: biniam
  Date: 4/3/2014
  Time: 5:35 PM
--%>
<%@ page import="com.bits.sart.model.Research" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'research.label', default: 'Research')}" />
    <title><g:message code="default.search.label" args="[entityName]" /> </title>
</head>

<body>
    <a href="#search-research" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
    <div class="nav" role="navigation">
        <ul>
            <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
        </ul>
    </div>
    <div id=""search-research" class="content scaffold-search" role="main">
        <h1><g:message code="default.search.label" args="[entityName]" /> </h1>

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

        <g:form action="search">
            <div class="search">
                <div class="researchTitle">
                    <label for="researchTitle">
                        <g:message code="research.search.title" default="Research Title" />
                    </label>
                    <g:textField name="researchTitle"/>
                </div>

                <div class="researchCategory">
                    <label for="researchCategory">
                        <g:message code="research.search.category" default="Research Category" />
                    </label>
                    <g:select name="researchCategory" from="${com.bits.sart.model.ResearchCategory.list()}" optionKey="id" optionValue="name" required="" class="many-to-one" />
                </div>

                <div class="buttons">
                    <g:submitButton name="searchButton"><g:message code="research.search.button" default="Search" /> </g:submitButton>
                </div>
            </div>
        </g:form>

        <table>
            <thead>
            <tr>

                <th><g:message code="research.researchCategory.label" default="Research Category" /></th>

                <g:sortableColumn property="title" title="${message(code: 'research.title.label', default: 'Title')}" />

                <th><g:message code="research.client.label" default="Client" /></th>

                <th><g:message code="research.projectManager.label" default="Project Manager" /></th>

                <g:sortableColumn property="budget" title="${message(code: 'research.budget.label', default: 'Budget')}" />

                <g:sortableColumn property="methodology" title="${message(code: 'research.methodology.label', default: 'Methodology')}" />

            </tr>
            </thead>
            <tbody>
                <g:each in="${searchResults}" status="i" var="searchResult">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                        <td><g:link action="show" id="${searchResult.id}">${fieldValue(bean: searchResult, field: "researchCategory")}</g:link></td>

                        <td>${fieldValue(bean: searchResult, field: "title")}</td>

                        <td>${fieldValue(bean: searchResult, field: "client")}</td>

                        <td>${fieldValue(bean: searchResult, field: "projectManager")}</td>

                        <td>${fieldValue(bean: searchResult, field: "budget")}</td>

                        <td>${fieldValue(bean: searchResult, field: "methodology")}</td>

                    </tr>
                </g:each>
            </tbody>
        </table>

        <div class="pagination">
            <g:paginate total="${researchInstanceCount ?: 0}" />
        </div>

    </div>

</body>
</html>