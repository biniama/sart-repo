
<%@ page import="com.bits.sart.model.Research" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'research.label', default: 'Research')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-research" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-research" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="research.researchCategory.label" default="Research Category" /></th>

                        <g:sortableColumn property="title" title="${message(code: 'research.title.label', default: 'Title')}" />

                        <th><g:message code="research.client.label" default="Client" /></th>

                        <th><g:message code="research.projectManager.label" default="Project Manager" /></th>

                        <g:sortableColumn property="budget" title="${message(code: 'research.budget.label', default: 'Budget')}" />

                        <g:sortableColumn property="methodology" title="${message(code: 'research.methodology.label', default: 'Methodology')}" />

						%{--<g:sortableColumn property="sampleSize" title="${message(code: 'research.sampleSize.label', default: 'Sample Size')}" />--}%

					</tr>
				</thead>
				<tbody>
				<g:each in="${researchInstanceList}" status="i" var="researchInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${researchInstance.id}">${fieldValue(bean: researchInstance, field: "researchCategory")}</g:link></td>

                        <td>${fieldValue(bean: researchInstance, field: "title")}</td>

						<td>${fieldValue(bean: researchInstance, field: "client")}</td>

                        <td>${fieldValue(bean: researchInstance, field: "projectManager")}</td>

                        <td>${fieldValue(bean: researchInstance, field: "budget")}</td>

                        <td>${fieldValue(bean: researchInstance, field: "methodology")}</td>

                        %{--<td>${fieldValue(bean: researchInstance, field: "sampleSize")}</td>--}%

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${researchInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
