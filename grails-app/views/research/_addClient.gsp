<%@ page import="com.bits.sart.model.Client" %>
<%@ page import="com.bits.sart.model.Address" %>

<div class ="clientInformation">
    <div class="clientName">
        <label for="clientName">
            <g:message code="client.name.label" default="Name" />
        </label>
        <g:textField name="clientName"/>
    </div>

    <div class="street required">
        <label for="street">
            <g:message code="address.street.label" default="Street" />
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="street" maxlength="100" required=""/>
    </div>

    <div class="woreda ">
        <label for="woreda">
            <g:message code="address.woreda.label" default="Woreda" />
        </label>
        <g:textField name="woreda" />
    </div>

    <div class="phoneNumber">
        <label for="phoneNumber">
            <g:message code="address.phoneNumber.label" default="Phone Number" />

        </label>
        <g:textField name="phoneNumber" />
    </div>

    <div class="city">
        <label for="city">
            <g:message code="address.city.label" default="City" />
        </label>
        <g:select id="city" name="city.id" from="${com.bits.sart.model.City.list()}" optionKey="id" optionValue="name" class="many-to-one" noSelection="['null': '']"/>
    </div>
</div>