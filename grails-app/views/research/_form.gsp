<%@ page import="com.bits.sart.model.Research" %>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'researchCategory', 'error')} required">
	<label for="researchCategory">
		<g:message code="research.researchCategory.label" default="Research Category" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="researchCategory" name="researchCategory.id" from="${com.bits.sart.model.ResearchCategory.list()}" optionKey="id" optionValue="name" required="" value="${researchInstance?.researchCategory?.name}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'client', 'error')} required">
    <label for="client">
        <g:message code="research.client.label" default="Client" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="client" name="client.id" from="${com.bits.sart.model.Client.list()}" optionKey="id" optionValue="name" required="" value="${researchInstance?.client?.name}" class="many-to-one"/>

    <div class="newClientForm"></div>
    <form method="post">
        <input type="button" value="Add Client" onclick="addClientTemplate();">
    </form>
</div>

%{--<div class ="clientInformation">
    <g:render template="addClient" />
</div>--}%



%{--
<div class ="clientInformation">
    <g:submitButton name="addClient" onclick="showAddClient();">
        <g:message code="research.add.client.label" default="Add Client" />
    </g:submitButton>
</div>

        <g:remoteLink action="addClient" update="addClientInResearchPage" onLoading="showSpinner(true);" onComplete="showSpinner(false);">
            <g:message code="research.add.client.label" default="Add Client" />
        </g:remoteLink>
        <g:render template="addClient" />
        <div id="addClientInResearchPage">
        </div>
--}%



<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="research.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${researchInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="research.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${researchInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'projectManager', 'error')} required">
	<label for="projectManager">
		<g:message code="research.projectManager.label" default="Project Manager" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="projectManager" name="projectManager.id" from="${com.bits.sart.model.SartUser.list()}" optionKey="id" optionValue="firstName" required="" value="${researchInstance?.projectManager?.firstName + " " + researchInstance?.projectManager?.lastName}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'sampleSize', 'error')} ">
	<label for="sampleSize">
		<g:message code="research.sampleSize.label" default="Sample Size" />
		
	</label>
	<g:textField name="sampleSize" value="${researchInstance?.sampleSize}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'responsibility', 'error')} ">
	<label for="responsibility">
		<g:message code="research.responsibility.label" default="Responsibility" />
		
	</label>
	<g:textField name="responsibility" value="${researchInstance?.responsibility}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'methodology', 'error')} required">
	<label for="methodology">
		<g:message code="research.methodology.label" default="Methodology" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="methodology" name="methodology.id" from="${com.bits.sart.model.Methodology.list()}" optionKey="id" optionValue="name" required="" value="${researchInstance?.methodology?.name}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'methodologyDescription', 'error')} ">
	<label for="methodologyDescription">
		<g:message code="research.methodologyDescription.label" default="Methodology Description" />
		
	</label>
	<g:textField name="methodologyDescription" value="${researchInstance?.methodologyDescription}"/>
</div>

%{--Duration--}%

    <div class="fieldcontain ${hasErrors(bean: researchInstance.duration, field: 'startDate', 'error')} required">
        <label for="startDate">
            <g:message code="duration.startDate.label" default="Start Date" />
            <span class="required-indicator">*</span>
        </label>
        <g:datePicker name="startDate" precision="day"  value="${researchInstance.duration?.startDate}"  />
    </div>

    <div class="fieldcontain ${hasErrors(bean: researchInstance.duration, field: 'endDate', 'error')} required">
        <label for="endDate">
            <g:message code="duration.endDate.label" default="End Date" />
            <span class="required-indicator">*</span>
        </label>
        <g:datePicker name="endDate" precision="day"  value="${researchInstance.duration?.endDate}"  />
    </div>

%{--End of Duration--}%

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'budget', 'error')} ">
	<label for="budget">
		<g:message code="research.budget.label" default="Budget" />
		
	</label>
	<g:field type="" name="budget" value="${fieldValue(bean: researchInstance, field: 'budget')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'geographicalCoverage', 'error')} required">
	<label for="geographicalCoverage">
		<g:message code="research.geographicalCoverage.label" default="Geographical Coverage" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="geographicalCoverage" from="${com.bits.sart.model.City.list()}" multiple="multiple" optionKey="id" optionValue="name" size="5" required="" value="${researchInstance?.geographicalCoverage*.name}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'researchers', 'error')} required">
	<label for="researchers">
		<g:message code="research.researchers.label" default="Researchers" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="researchers" from="${com.bits.sart.model.Researcher.list()}" multiple="multiple" optionKey="id" optionValue="sartUser" size="5" required="" value="${researchInstance?.researchers?.sartUser?.firstName}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance?.attachments, field: 'description', 'error')} ">
    <label for="description">
        <g:message code="attachment.description.label" default="Description" />

    </label>
    <g:textField name="description" value="${researchInstance?.attachments?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance?.attachments, field: 'fileName', 'error')} required">
    <label for="fileName">
        <g:message code="attachment.fileName.label" default="File Name" />
        <span class="required-indicator">*</span>
    </label>
    <input type="file" id="fileName" name="fileName" />
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance?.attachments, field: 'type', 'error')} ">
    <label for="type">
        <g:message code="attachment.type.label" default="Type" />

    </label>
    <g:textField name="type" value="${researchInstance?.attachments?.type}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'softwareUsed', 'error')} ">
	<label for="softwareUsed">
		<g:message code="research.softwareUsed.label" default="Software Used" />
		
	</label>
	<g:textField name="softwareUsed" value="${researchInstance?.softwareUsed}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'researchReferences', 'error')} ">
	<label for="researchReferences">
		<g:message code="research.researchReferences.label" default="Research References" />
		
	</label>
	<g:textField name="researchReferences" value="${researchInstance?.researchReferences}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'externalCollaborators', 'error')} ">
	<label for="externalCollaborators">
		<g:message code="research.externalCollaborators.label" default="External Collaborators" />
		
	</label>
	<g:textField name="externalCollaborators" value="${researchInstance?.externalCollaborators}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'remark', 'error')} ">
	<label for="remark">
		<g:message code="research.remark.label" default="Remark" />
		
	</label>
	<g:textField name="remark" value="${researchInstance?.remark}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'lessonsLearned', 'error')} ">
	<label for="lessonsLearned">
		<g:message code="research.lessonsLearned.label" default="Lessons Learned" />
		
	</label>
	<g:textField name="lessonsLearned" value="${researchInstance?.lessonsLearned}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'createdByUser', 'error')} ">
	<label for="createdByUser">
		<g:message code="research.createdByUser.label" default="Created By" />
		
	</label>
	<g:select id="createdByUser" name="createdByUser.id" from="${com.bits.sart.model.User.list()}" optionKey="id" optionValue="username" value="${researchInstance?.createdByUser?.username}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: researchInstance, field: 'updatedByUser', 'error')} ">
	<label for="updatedByUser">
		<g:message code="research.updatedByUser.label" default="Updated By" />
		
	</label>
	<g:select id="updatedByUser" name="updatedByUser.id" from="${com.bits.sart.model.User.list()}" optionKey="id" optionValue="username" value="${researchInstance?.updatedByUser?.username}" class="many-to-one" noSelection="['null': '']"/>
</div>

