
<%@ page import="com.bits.sart.model.Duration" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'duration.label', default: 'Duration')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-duration" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-duration" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list duration">
			
				<g:if test="${durationInstance?.createdByUser}">
				<li class="fieldcontain">
					<span id="createdByUser-label" class="property-label"><g:message code="duration.createdByUser.label" default="Created By User" /></span>
					
						<span class="property-value" aria-labelledby="createdByUser-label"><g:link controller="user" action="show" id="${durationInstance?.createdByUser?.id}">${durationInstance?.createdByUser?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${durationInstance?.updatedByUser}">
				<li class="fieldcontain">
					<span id="updatedByUser-label" class="property-label"><g:message code="duration.updatedByUser.label" default="Updated By User" /></span>
					
						<span class="property-value" aria-labelledby="updatedByUser-label"><g:link controller="user" action="show" id="${durationInstance?.updatedByUser?.id}">${durationInstance?.updatedByUser?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${durationInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="duration.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${durationInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${durationInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="duration.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${durationInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${durationInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="duration.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${durationInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${durationInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="duration.startDate.label" default="Start Date" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${durationInstance?.startDate}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:durationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${durationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
