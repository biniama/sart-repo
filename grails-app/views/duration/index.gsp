
<%@ page import="com.bits.sart.model.Duration" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'duration.label', default: 'Duration')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-duration" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-duration" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="duration.createdByUser.label" default="Created By User" /></th>
					
						<th><g:message code="duration.updatedByUser.label" default="Updated By User" /></th>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'duration.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="endDate" title="${message(code: 'duration.endDate.label', default: 'End Date')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'duration.lastUpdated.label', default: 'Last Updated')}" />
					
						<g:sortableColumn property="startDate" title="${message(code: 'duration.startDate.label', default: 'Start Date')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${durationInstanceList}" status="i" var="durationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${durationInstance.id}">${fieldValue(bean: durationInstance, field: "createdByUser")}</g:link></td>
					
						<td>${fieldValue(bean: durationInstance, field: "updatedByUser")}</td>
					
						<td><g:formatDate date="${durationInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${durationInstance.endDate}" /></td>
					
						<td><g:formatDate date="${durationInstance.lastUpdated}" /></td>
					
						<td><g:formatDate date="${durationInstance.startDate}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${durationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
