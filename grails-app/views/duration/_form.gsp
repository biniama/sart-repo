<%@ page import="com.bits.sart.model.Duration" %>



<div class="fieldcontain ${hasErrors(bean: durationInstance, field: 'createdByUser', 'error')} ">
	<label for="createdByUser">
		<g:message code="duration.createdByUser.label" default="Created By User" />
		
	</label>
	<g:select id="createdByUser" name="createdByUser.id" from="${com.bits.sart.model.User.list()}" optionKey="id" value="${durationInstance?.createdByUser?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: durationInstance, field: 'updatedByUser', 'error')} ">
	<label for="updatedByUser">
		<g:message code="duration.updatedByUser.label" default="Updated By User" />
		
	</label>
	<g:select id="updatedByUser" name="updatedByUser.id" from="${com.bits.sart.model.User.list()}" optionKey="id" value="${durationInstance?.updatedByUser?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: durationInstance, field: 'endDate', 'error')} required">
	<label for="endDate">
		<g:message code="duration.endDate.label" default="End Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="endDate" precision="day"  value="${durationInstance?.endDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: durationInstance, field: 'startDate', 'error')} required">
	<label for="startDate">
		<g:message code="duration.startDate.label" default="Start Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="startDate" precision="day"  value="${durationInstance?.startDate}"  />
</div>

