<%@ page import="com.bits.sart.model.Attachment" %>

<div class="content scaffold-create" role="main">
    <h1>New Upload</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:uploadForm action="upload">
        <fieldset class="form">
            <input type="file" name="attachedFile" />
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="upload" class="save" value="Upload" />
        </fieldset>
    </g:uploadForm>
</div>