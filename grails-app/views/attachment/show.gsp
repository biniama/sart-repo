
<%@ page import="com.bits.sart.model.Attachment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'attachment.label', default: 'Attachment')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-attachment" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-attachment" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list attachment">
			
				<g:if test="${attachmentInstance?.attachmentDescription}">
				<li class="fieldcontain">
					<span id="attachmentDescription-label" class="property-label"><g:message code="attachment.attachmentDescription.label" default="Attachment Description" /></span>
					
						<span class="property-value" aria-labelledby="attachmentDescription-label"><g:fieldValue bean="${attachmentInstance}" field="attachmentDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="attachment.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${attachmentInstance}" field="type"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.fileName}">
				<li class="fieldcontain">
					<span id="fileName-label" class="property-label"><g:message code="attachment.fileName.label" default="fileName" /></span>
					
						<span class="property-value" aria-labelledby="fileName-label"><g:fieldValue bean="${attachmentInstance}" field="fileName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.fullPath}">
				<li class="fieldcontain">
					<span id="fullPath-label" class="property-label"><g:message code="attachment.fullPath.label" default="Full Path" /></span>
					
						<span class="property-value" aria-labelledby="fullPath-label"><g:fieldValue bean="${attachmentInstance}" field="fullPath"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.createdByUser}">
				<li class="fieldcontain">
					<span id="createdByUser-label" class="property-label"><g:message code="attachment.createdByUser.label" default="Created By User" /></span>
					
						<span class="property-value" aria-labelledby="createdByUser-label"><g:link controller="user" action="show" id="${attachmentInstance?.createdByUser?.id}">${attachmentInstance?.createdByUser?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.updatedByUser}">
				<li class="fieldcontain">
					<span id="updatedByUser-label" class="property-label"><g:message code="attachment.updatedByUser.label" default="Updated By User" /></span>
					
						<span class="property-value" aria-labelledby="updatedByUser-label"><g:link controller="user" action="show" id="${attachmentInstance?.updatedByUser?.id}">${attachmentInstance?.updatedByUser?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="attachment.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${attachmentInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${attachmentInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="attachment.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${attachmentInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:attachmentInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${attachmentInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
