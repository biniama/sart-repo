<%@ page import="com.bits.sart.model.Client" %>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'clientName', 'error')} required">
	<label for="clientName">
		<g:message code="client.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="clientName" required="" value="${clientInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'address', 'error')} required">
    <label for="street">
        <g:message code="client.address.street.label" default="Street" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="street" maxlength="100" required="" value="${clientInstance?.address?.street}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance.address, field: 'woreda', 'error')} ">
    <label for="woreda">
        <g:message code="client.address.woreda.label" default="Woreda" />
    </label>
    <g:textField name="woreda" value="${clientInstance?.address?.woreda}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance.address, field: 'phoneNumber', 'error')} ">
    <label for="phoneNumber">
        <g:message code="client.address.phoneNumber.label" default="Phone Number" />

    </label>
    <g:textField name="phoneNumber" value="${clientInstance?.address?.phoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance.address, field: 'email', 'error')} ">
    <label for="email">
        <g:message code="client.address.email.label" default="Email" />

    </label>
    <g:textField name="email" value="${clientInstance?.address?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance.address, field: 'city', 'error')} ">
    <label for="cityId">
        <g:message code="client.address.city.label" default="City" />
    </label>
    <g:select id="cityId" name="cityId" from="${com.bits.sart.model.City.list()}" optionKey="id" optionValue="name" value="${clientInstance?.address?.city?.name}" class="many-to-one" noSelection="['null': '']"/>
</div>

