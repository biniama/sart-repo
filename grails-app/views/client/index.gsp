
<%@ page import="com.bits.sart.model.Client" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'client.label', default: 'Client')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-client" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-client" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'client.name.label', default: 'Name')}" />

                        <g:sortableColumn property="address.street" title="${message(code: 'client.address.street.label', default: 'Street')}" />

                        <g:sortableColumn property="address.woreda" title="${message(code: 'client.address.woreda.label', default: 'Woreda')}" />

                        <g:sortableColumn property="address.phoneNumber" title="${message(code: 'client.address.phoneNumber.label', default: 'Phone Number')}" />

                        <g:sortableColumn property="address.city" title="${message(code: 'client.address.city.label', default: 'City')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${clientInstanceList}" status="i" var="clientInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${clientInstance.id}">${fieldValue(bean: clientInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: clientInstance, field: "address.street")}</td>

                        <td>${fieldValue(bean: clientInstance, field: "address.woreda")}</td>

                        <td>${fieldValue(bean: clientInstance, field: "address.phoneNumber")}</td>

                        <td>${fieldValue(bean: clientInstance, field: "address.city.name")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${clientInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
