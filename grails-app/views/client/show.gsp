
<%@ page import="com.bits.sart.model.Client" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'client.label', default: 'Client')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-client" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-client" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list client">
			
				<g:if test="${clientInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="client.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${clientInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.address.street}">
				<li class="fieldcontain">
					<span id="address.street-label" class="property-label">
                        <g:message code="client.address.street.label" default="Street" />
                    </span>
					
					<span class="property-value" aria-labelledby="address.street-label">
                        <g:fieldValue bean="${clientInstance.address}" field="street"/>
                    </span>
				</li>
				</g:if>

                <g:if test="${clientInstance?.address.woreda}">
                    <li class="fieldcontain">
                        <span id="address.woreda-label" class="property-label">
                            <g:message code="client.address.woreda.label" default="Woreda" />
                        </span>

                        <span class="property-value" aria-labelledby="address.woreda-label">
                            <g:fieldValue bean="${clientInstance.address}" field="woreda"/>
                        </span>
                    </li>
                </g:if>

                <g:if test="${clientInstance?.address.phoneNumber}">
                    <li class="fieldcontain">
                        <span id="address.phoneNumber-label" class="property-label">
                            <g:message code="client.address.phoneNumber.label" default="Phone Number" />
                        </span>

                        <span class="property-value" aria-labelledby="address.phoneNumber-label">
                            <g:fieldValue bean="${clientInstance.address}" field="phoneNumber"/>
                        </span>
                    </li>
                </g:if>

                <g:if test="${clientInstance?.address.city}">
                    <li class="fieldcontain">
                        <span id="address.city-label" class="property-label">
                            <g:message code="client.address.city.label" default="City" />
                        </span>

                        <span class="property-value" aria-labelledby="address.city-label">
                            <g:fieldValue bean="${clientInstance.address.city}" field="name"/>
                        </span>
                    </li>
                </g:if>
			</ol>
			<g:form url="[resource:clientInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${clientInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
