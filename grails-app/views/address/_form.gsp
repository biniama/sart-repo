<%@ page import="com.bits.sart.model.Address" %>



<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'street', 'error')} required">
	<label for="street">
		<g:message code="address.street.label" default="Street" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="street" maxlength="100" required="" value="${addressInstance?.street}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'woreda', 'error')} ">
	<label for="woreda">
		<g:message code="address.woreda.label" default="Woreda" />
		
	</label>
	<g:textField name="woreda" value="${addressInstance?.woreda}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'phoneNumber', 'error')} ">
	<label for="phoneNumber">
		<g:message code="address.phoneNumber.label" default="Phone Number" />
		
	</label>
	<g:textField name="phoneNumber" value="${addressInstance?.phoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="address.city.label" default="City" />
		
	</label>
	<g:select id="city" name="city.id" from="${com.bits.sart.model.City.list()}" optionKey="id" value="${addressInstance?.city?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'createdByUser', 'error')} ">
	<label for="createdByUser">
		<g:message code="address.createdByUser.label" default="Created By User" />
		
	</label>
	<g:select id="createdByUser" name="createdByUser.id" from="${com.bits.sart.model.User.list()}" optionKey="id" value="${addressInstance?.createdByUser?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'updatedByUser', 'error')} ">
	<label for="updatedByUser">
		<g:message code="address.updatedByUser.label" default="Updated By User" />
		
	</label>
	<g:select id="updatedByUser" name="updatedByUser.id" from="${com.bits.sart.model.User.list()}" optionKey="id" value="${addressInstance?.updatedByUser?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

