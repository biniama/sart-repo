CREATE DATABASE IF NOT EXISTS sart_repo;

USE mysql;

create user sart_repo_admin IDENTIFIED BY 'sart+admin-p@55w0rd';
create user sart_repo_admin@localhost IDENTIFIED BY 'sart+admin-p@55w0rd';

GRANT ALL ON sart_repo.* TO 'sart_repo_admin' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
GRANT ALL ON sart_repo.* TO 'sart_repo_admin'@'%' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
GRANT ALL ON sart_repo.* TO 'sart_repo_admin'@'localhost' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;

GRANT GRANT OPTION ON sart_repo.* TO 'sart_repo_admin' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
GRANT GRANT OPTION ON sart_repo.* TO 'sart_repo_admin'@'%' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
GRANT GRANT OPTION ON sart_repo.* TO 'sart_repo_admin'@'localhost' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;

GRANT ALL ON `mysql`.`proc` TO sart_repo_admin;
GRANT ALL ON `mysql`.`proc` TO sart_repo_admin@'%';
GRANT ALL ON `mysql`.`proc` TO sart_repo_admin@'localhost';

flush privileges;

/**
	drop database sart;
	drop user sart_admin;
	drop user sart_admin@localhost;
*/

/** Script to Create Database, Create User and Grant permissions of database to user:
		CREATE DATABASE bg_ict_training;
		CREATE USER 'bgict'@'localhost' IDENTIFIED BY 'bgict';
		GRANT ALL ON bg_ict_training.* to 'bgict'@'localhost';

	// TO drop the database and user:
		-- DROP DATABASE bg_ict_training;
		-- DROP USER 'bgict'@'localhost';
 */