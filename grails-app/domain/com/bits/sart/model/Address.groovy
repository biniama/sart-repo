package com.bits.sart.model

class Address extends AbstractDomain {

	String street
	
	String woreda
	
	String phoneNumber

    String email

	City city

	Date dateCreated
	Date lastUpdated
	User createdByUser
	User updatedByUser
	
	static constraints = {
		
		street blank:false, maxSize:100
        woreda  nullable: true
        phoneNumber nullable: true
        email nullable: true
        city nullable: true

		createdByUser nullable:true
		updatedByUser nullable:true
	}
}
