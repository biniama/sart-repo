package com.bits.sart.model

/**
 * Created by biniam on 5/2/2014.
 */
class AttachmentType extends AbstractDomain {

    String type

    Date dateCreated
    Date lastUpdated
    User createdByUser
    User updatedByUser

    static constraints = {

        type nullable: true

        createdByUser nullable: true
        updatedByUser nullable: true
    }
}