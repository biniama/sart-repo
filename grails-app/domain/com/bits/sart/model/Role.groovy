package com.bits.sart.model

class Role extends AbstractDomain {

	String authority

    Date dateCreated
    Date lastUpdated
    User createdByUser
    User updatedByUser

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true

        createdByUser nullable:true
        updatedByUser nullable:true
    }
}
