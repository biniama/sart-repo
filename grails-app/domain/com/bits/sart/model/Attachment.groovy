package com.bits.sart.model

class Attachment extends AbstractDomain {

    String fileName

    String fullPath

    AttachmentType type 			// {questionnaire, proposal, report} {can be Type type}
	
	String attachmentDescription

    Date dateCreated
    Date lastUpdated
    User createdByUser
    User updatedByUser

    //static belongsTo = [sartUser : SartUser]    //, research : Research]

    static constraints = {

        attachmentDescription nullable: true
        type nullable: true

        fileName(blank:false,nullable:false)
        fullPath(blank:false,nullable:false)

       // sartUser nullable: true

        createdByUser nullable:true
        updatedByUser nullable:true
    }


}
