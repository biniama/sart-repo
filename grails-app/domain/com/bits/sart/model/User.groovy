package com.bits.sart.model

class User extends AbstractDomain {

	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

    Date dateCreated
    Date lastUpdated
    User createdByUser
    User updatedByUser

    static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false

        createdByUser nullable:true
        updatedByUser nullable:true
	}

	static mapping = {
		password column: '`password`'
        table '`user`'
        id generator: 'identity'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {

        super.beforeInsert()
		encodePassword()
	}

	def beforeUpdate() {

        super.beforeUpdate()

		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
