package com.bits.sart.model

class City extends AbstractDomain {

	String name
	
	Date dateCreated
	Date lastUpdated
	User createdByUser
	User updatedByUser
	
	//static belongsTo = [research : Research, address: Address]
	
	static constraints = {

        name unique: true, blank: false
		createdByUser nullable:true
		updatedByUser nullable:true
	}

}
