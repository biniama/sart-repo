package com.bits.sart.model

class SartUser extends User {

	String firstName
	String middleName
	String lastName
	
	String summary
	String position							//	{can be Enum}
	
	GenderTypeEnum gender
	Address address
	
	LevelOfEducation highestLevelOfEducation
	Double yearsOfExperience

    Attachment attachment

	Date dateCreated
	Date lastUpdated
	User createdByUser
	User updatedByUser

	//static hasMany = [attachments : Attachment]
	
	static constraints = {

        firstName blank: false
        middleName blank: false
        lastName blank: false

        summary blank: true
        position blank: false

        gender nullable: true
        address nullable: true

        highestLevelOfEducation nullable: true
        yearsOfExperience nullable: true

        attachment nullable: true
        //attachments nullable: true

		createdByUser nullable:true
		updatedByUser nullable:true
	}
	
}
