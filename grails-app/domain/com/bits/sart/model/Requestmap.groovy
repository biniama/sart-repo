package com.bits.sart.model

import org.springframework.http.HttpMethod

class Requestmap extends AbstractDomain {

	String url
	String configAttribute
	HttpMethod httpMethod

    Date dateCreated
    Date lastUpdated
    User createdByUser
    User updatedByUser

	static mapping = {
		cache true
	}

	static constraints = {
		url blank: false, unique: 'httpMethod'
		configAttribute blank: false
		httpMethod nullable: true

        createdByUser nullable:true
        updatedByUser nullable:true
	}
}
