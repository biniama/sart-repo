/*
 To use MyISAM, your configuration will look like this:
 dataSource {
 pooled = true
 driverClassName = "com.mysql.jdbc.Driver"
 dialect = 'org.hibernate.dialect.MySQL5Dialect'
 }
 To use InnoDB, your configuration should look like:
 dataSource {
 pooled = true
 driverClassName = "com.mysql.jdbc.Driver"
 dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
 }
 */

dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
    url = "jdbc:mysql://sartserver:3306/sart_repo?useUnicode=yes&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull"

    // Reconnect to mysql automatically
    // http://andrzejgrzesik.info/2011/12/18/grails-automatically-reconnect-to-mysql-datasource/
    properties {
        maxActive = 50
        maxIdle = 25
        minIdle = 1
        initialSize = 1

        numTestsPerEvictionRun = 3
        maxWait = 10000

        testOnBorrow = true
        testWhileIdle = true
        testOnReturn = true

        validationQuery = "select now()"

        minEvictableIdleTimeMillis = 1000 * 60 * 5
        timeBetweenEvictionRunsMillis = 1000 * 60 * 5
    }
}


hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "create" // one of 'create', 'create-drop', 'update', 'validate', ''
            username = "sart_repo_admin"
            password = "sart+admin-p@55w0rd"
           // logSql = true
           // formatSql = true
        }
    }
    test {
        dataSource {
            username = "sart_repo_admin"
            password = "sart+admin-p@55w0rd"
            pooled = true
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
    production {
        dataSource {
            username = "sart_repo_admin"
            password = "sart+admin-p@55w0rd"
            //password = "S6+a0hn9wyOiBS/nt/iB9Q=="
            //passwordEncryptionCodec = DESCodec
            pooled = true
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
}
