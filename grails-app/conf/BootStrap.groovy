import com.bits.sart.Constants
import com.bits.sart.model.City
import com.bits.sart.model.Methodology
import com.bits.sart.model.Requestmap
import com.bits.sart.model.ResearchCategory
import com.bits.sart.model.Role
import com.bits.sart.model.User
import com.bits.sart.model.UserRole

class BootStrap {

    def init = { servletContext ->

            // Create roles

            def adminRole = Role.findOrSaveByAuthority(Constants.ROLE_ADMIN).save()

            def researcherRole = Role.findOrSaveByAuthority(Constants.ROLE_RESEARCHER).save()

            def hrRole = Role.findOrSaveByAuthority(Constants.ROLE_HR).save()

            // Create users
            def adminUser = User.findOrSaveByUsernameAndPassword('admin', 'password').save()
            def researcherUser = User.findOrSaveByUsernameAndPassword('researcher', 'password').save()
            def hrUser = User.findOrSaveByUsernameAndPassword('hr', 'password').save()

            // Assign roles to users
            UserRole.create adminUser, adminRole, true
            UserRole.create researcherUser, researcherRole, true
            UserRole.create hrUser, hrRole, true

            // Request map Stored in Database

            for (String url in [
                    '/', '/index', '/index.gsp', 'favicon.ico',
                    'js*', 'css*', 'images*',
                    '/login', '/login*//**',
                    '/logout', '/logout*//**'
                ]) {
                    Requestmap.findOrSaveByUrlAndConfigAttribute(url, 'permitAll').save()
                }

            Requestmap.findOrSaveByUrlAndConfigAttribute('/research*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()
            Requestmap.findOrSaveByUrlAndConfigAttribute('/client*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()
            Requestmap.findOrSaveByUrlAndConfigAttribute('/city*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()
            Requestmap.findOrSaveByUrlAndConfigAttribute('/address*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()
            Requestmap.findOrSaveByUrlAndConfigAttribute('/attachment*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()
            Requestmap.findOrSaveByUrlAndConfigAttribute('/duration*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()
            Requestmap.findOrSaveByUrlAndConfigAttribute('/sartUser*//**', 'ROLE_ADMIN, ROLE_RESEARCHER').save()

            Requestmap.findOrSaveByUrlAndConfigAttribute('/j_spring_security_switch_user', 'ROLE_SWITCH_USER, isFullyAuthenticated()').save()

            /*
                new Requestmap(url: '/profile*',    configAttribute: 'ROLE_USER').save()
                new Requestmap(url: '/admin*',      configAttribute: 'ROLE_ADMIN').save()
                new Requestmap(url: '/admin/role*', configAttribute: 'ROLE_SUPERVISOR').save()
                new Requestmap(url: '/admin/user*', configAttribute: 'ROLE_ADMIN,ROLE_SUPERVISOR').save()
            */

            // Add Research Categories
            ResearchCategory.findOrSaveByNameAndDescription('Market Research', 'Market Research').save()
            ResearchCategory.findOrSaveByNameAndDescription('Social Research', 'Social Research').save()

            // Add Cities
            for(String cityName in ['Addis Ababa', 'Gondar', 'Harar', 'Dire Dawa', 'Bahir Dar',
                                'Hawassa', 'Arba Minch', 'Adama', 'Mekelle']) {

                City.findOrSaveByName(cityName).save()
            }

            // Add Methodologies
            Methodology.findOrSaveByNameAndDescription('Qualitative', 'Qualitative research').save()
            Methodology.findOrSaveByNameAndDescription('Quantitative', 'Quantitative research').save()
            Methodology.findOrSaveByNameAndDescription('Mixed', 'Mixed (both Qualitative and Quantitative) research').save()

            /*
            if (Role.findByAuthority(Constants.ROLE_ADMIN) == null)
                 def adminRole = new Role(authority: Constants.ROLE_ADMIN).save(flush: true)

             if (Role.findByAuthority(Constants.ROLE_RESEARCHER) == null)
                 def researcherRole = new Role(authority: Constants.ROLE_RESEARCHER).save(flush: true)

             if (Role.findByAuthority(Constants.ROLE_HR) == null)
                 def hrRole = new Role(authority: Constants.ROLE_HR).save(flush: true)

             if (User.findByUsername(Constants.USER_ADMIN) == null) {

                 def adminUser = new User(username: 'admin', password: 'password').save(flush: true)
                 UserRole.create adminUser, adminRole, true
             }


               for (String url in [
                       '/', '/index', '/index.gsp', 'favicon.ico',
                       'js*', 'css*', 'images*',
                       '/login', '/login.*', '/login',
                       '/logout', '/logout.*', '/logout']) {

                   if(Requestmap.findByUrl(url) == null)
                       new Requestmap(url: url, configAttribute: 'permitAll').save()
               }

               if(Requestmap.findByUrl('/research*') == null)
                   new Requestmap(url: '/research*', configAttribute: 'ROLE_ADMIN, ROLE_RESEARCHER').save()


               // Add Research Categories
               if(ResearchCategory.findByName('Market Research') == null)
                   new ResearchCategory(name: 'Market Research', description: 'Market Research').save()

               if(ResearchCategory.findByName('Social Research') == null)
                   new ResearchCategory(name: 'Social Research', description: 'Social Research').save()

               // Add Cities
               for(String cityName in ['Addis Ababa', 'Gondar', 'Harar', 'Dire Dawa', 'Bahir Dar',
                                       'Hawassa', 'Arba Minch', 'Adama', 'Mekelle']) {
                   if(City.findByName(cityName) == null)
                       new City(name: cityName).save()
               }

               // Add Methodologies
               if(Methodology.findByName('Qualitative') == null)
                   new Methodology(name: 'Qualitative', description: 'Qualitative').save()

               if(Methodology.findByName('Quantitative') == null)
                   new Methodology(name: 'Quantitative', description: 'Quantitative').save()

               if(Methodology.findByName('Mixed') == null)
                   new Methodology(name: 'Mixed', description: 'Mixed').save()
            */
    }

    def destroy = {}
}
