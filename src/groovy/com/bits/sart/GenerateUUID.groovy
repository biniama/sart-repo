package com.bits.sart

/**
 * Created by biniam on 4/2/2014.
 */
class GenerateUUIDGrails {

    def beforeInsert() {

        print UUID.randomUUID().toString()
    }

    /*
        class Widget {
            String uuid

            static constraints = {
                uuid unique: true
            }

            def beforeInsert() {
                // optionally, replace the dashes by adding .replaceAll('-','')
                uuid = UUID.randomUUID().toString()
            }
        }
        Then you could use a controller like so:

        // url: app/public/widget/48b5451a-0d21-4a36-bcc0-88b129852f1b

        PublicController {
            def widget() {
                Widget w = Widget.findByUuid(params.id)
                ...
            }
        }
    */
}
