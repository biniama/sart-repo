package com.bits.sart.model

/**
 * Created by biniam on 4/9/2014.
 */
public enum TypeEnum {

    CURRICULUM_VITAE,
    QUESTIONNAIRE,
    PROPOSAL,
    REPORT,
    OTHER
}