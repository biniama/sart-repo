package com.bits.sart.exception

/**
 * Created by biniam on 4/8/2014.
 */
class ServiceException extends RuntimeException {

    private final int code

    public ServiceException(String message, int code) {

        super(message)
        this.code = code
    }

    public int getCode() {
        return code
    }
}
