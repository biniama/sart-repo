package com.bits.sart.exception

/**
 * Created by biniam on 4/8/2014.
 */
class UserServiceException extends ServiceException {

    public static final int ERROR_PASSWORD_DOES_NOT_MATCH = 1901
    public static final int ERROR_USER_CANNOT_BE_CREATED = 1902

    def UserServiceException(String message, int code) {
        super(message, code)
    }
}
